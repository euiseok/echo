#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<netinet/in.h>
#include<sys/socket.h>
#include <stdlib.h>

void error_handling(char* msg)
{
	fputs(msg, stderr);
	fputc('\n', stderr);
	exit(1);
}

int main(int argc, char* argv[])
{

	int sockfd,n;
	struct sockaddr_in servaddr, cliaddr;
	socklen_t len;
	char mesg[1000];
	sockfd=socket(AF_INET, SOCK_DGRAM, 0); 
        int cnt_i;

        if (argc != 2)
        {
                fprintf(stderr, "Usage : %s <port>\n", argv[0]);
                exit(1);
        }

        memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family=AF_INET;
	servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
	servaddr.sin_port=htons(atoi(argv[1]));

	if(bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)))
        {
                error_handling("bind() error\n");
        }
        
        for (cnt_i = 0; cnt_i < 10; cnt_i++)
        {
                do
                {
                        len = sizeof(cliaddr);
                        n = recvfrom(sockfd, mesg, 1000, 0, (struct sockaddr *)&cliaddr, &len);
                        sendto(sockfd, mesg, n, 0, (struct sockaddr *)&cliaddr, sizeof(cliaddr));
                        mesg[n] = '\0';
                        printf("Received data: %s\n", mesg);
                } while (!strlen(mesg));

                
        }

        close(sockfd);
        return 0;

}
